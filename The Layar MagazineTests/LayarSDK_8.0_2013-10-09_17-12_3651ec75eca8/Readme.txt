Product:  
Layar SDK version 8.0

Hardware and software requirements
- iPhone 3GS or higher
- iPad 2 or higher
- iOS version 6.0 or higher
- Xcode 4.5 or higher
- iPhone SDK 6.0 or higher
- A published layer or campaign in your Layar account. For getting started please see http://devsupport.layar.com/forums/20604191-getting-started or http://devsupport.layar.com/forums/20603886-getting-started

License: 
By embedding the Layar SDK framework in your application, you agree to the Terms and Conditions described in the Layar_SDK_License_Agreement_vx_y_yyyymmdd.pdf file which can be found in the Layar SDK package or as Attachment 4 to our Layar Terms and Conditions for Developers and Publishers at http://layar.com/legal/terms-developer-publisher

Contents of this package:
- LayarSDK.framework: The actual framework 
- LayarSDK.framework/Resources/LayarSDKResources.bundle: The bundle with resources that you may change to your convenience
- Libraries: Includes external libraries used by the LayarSDK
- Layar SDK Documentation/html/index.html: start here for the documentation
- Layar SDK Documentation/html/com.layar.layarSDK.docset: the same documentation as a docset that you can add to XCode.
- Layar SDK Documentation/InstallDocSet.sh: shell script to install docset to XCode
- This Readme.txt file

Changelog:
- August 13th 2013 - SDK 8.0
Support for Geo layers is back.
A lot of new delegate methods added allowing easier customization.
Multiple bug-fixes and performance improvements.

- June 24th 2013 - SDK 7.2.4
Layar SDK has been supplied with delegate methods to receive callbacks for successfully scanned reference images and campaigns. A bug has been fixed that caused all statistics to be send twice.

- April 11th 2013 - SDK 7.2.3
Layar SDK now supports 5 additional languages: Dutch, German, Spanish, French and Japanese.

- March 15th 2013 - SDK 7.2.0
Layar SDK now supports QR-code scanning. Multiple bug-fixes and performance improvements.

- March 6th 2013 - SDK 7.1.2
Fixed crash to improve stability.

- February 14th 2013 - SDK 7.1.1
Fixed crash to improve stability.

- January 15th 2013 - SDK 7.1.0
Release of the LayarSDK.

Contact and Information:
- Layar: http://www.layar.com
- Layar SDK support:  http://devsupport.layar.com/categories/6169-layar-sdk
