//
//  ViewController.m
//  The Layar Magazine
//
//  Created by Olga Beza on 10/11/13.
//  Copyright (c) 2013 Layar. All rights reserved.
//

#import "ViewController.h"
#import <LayarSDK/LayarSDKViewController.h>

@interface ViewController ()

@end

@implementation ViewController
{
    UIButton* screenshotButton;
    UIButton* exitPopButton;
    UIButton* threeDTiltPopButton;
    UIButton* previewPopButton;
    UIButton* fitPopButton;
    
    //    UILabel *logLabel;
    UITextView *logText;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpSDK

//open ar view
{
    NSString *consumerKey = @"DZqzMYSAOKjliTPe"; // your OAuth key
    NSString *consumerSecret = @"UrSzTxnJRblCedhuPgmpGVcjMAvQqDsE"; // your OAuth secret
    
    if (!self.layarSDK)
    {
        self.layarSDK = [LayarSDK layarSDKWithConsumerKey:consumerKey andConsumerSecret:consumerSecret andDelegate:self];
    }
}

- (void) ShowLogs:(NSString*)logString
    onViewController:(UIViewController*)viewController
    {
        NSDate *currentTime = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"hh:mm:ss.SSS"];
        NSString *timeString = [dateFormatter stringFromDate: currentTime];
        NSString *joinedString = [NSString stringWithFormat:@"Time: %@\n%@; \n--------\n", timeString, logString];
        dispatch_async(dispatch_get_main_queue(), ^{
            logText.text=[logText.text stringByAppendingString:joinedString];//logString;
            [logText scrollRangeToVisible:NSMakeRange([logText.text length], 0)];
            [viewController.view addSubview:logText];
        });
        NSLog(@"%@", joinedString);
    }

- (void)openURL:(NSString*)url animated:(BOOL)animated
{
    [self openURL:url animated:animated fromViewController:self];
}

- (void)openURL:(NSString*)url animated:(BOOL)animated fromViewController:(UIViewController*)fromViewController
{
	[self setUpSDK];
	
	[self.layarSDK viewControllerForURL:[NSURL URLWithString:url] withCompletion:
	 ^(UIViewController<LayarSDKViewController> *viewController)
     {
         if (viewController)
         {
             if (viewController.layerSettingsAvailable)
             {
                 UIButton* filtersButton = [[UIButton alloc] initWithFrame:CGRectMake(190.0f, 55.0f, 80.0f, 40.0f)];
                 [filtersButton setTitle:@"Filters" forState:UIControlStateNormal];
                 [filtersButton addTarget:viewController action:@selector(showLayerSettings) forControlEvents:UIControlEventTouchUpInside];
                 [viewController.view addSubview:filtersButton];
             }
             
             if (viewController.mapViewAvailable)
             {
                 UIButton* mapButton = [[UIButton alloc] initWithFrame:CGRectMake(250.0f, 55.0f, 80.0f, 40.0f)];
                 [mapButton setTitle:@"Map" forState:UIControlStateNormal];
                 [mapButton addTarget:viewController action:@selector(showMapView) forControlEvents:UIControlEventTouchUpInside];
                 [viewController.view addSubview:mapButton];
             }
             
             // Logs
             logText = [[UITextView alloc] initWithFrame:CGRectMake(10, 85, 300, 200)];
             [logText setTextColor:[UIColor greenColor]];
             [logText setBackgroundColor:[UIColor clearColor]];
             [logText setFont:[UIFont fontWithName: @"Trebuchet MS" size: 12.0f]];
             [logText setEditable:false];
             [logText setSelectable:true];
             
             // "Hide Logs" button definition
             UIButton* hideButton = [[UIButton alloc] initWithFrame:CGRectMake(50.0f, 55.0f, 80.0f, 40.0f)];
             [hideButton setTitle:@"Hide" forState:UIControlStateNormal];
             [hideButton addTarget:self action:@selector(hideLogs) forControlEvents:UIControlEventTouchUpInside];
             [viewController.view addSubview:hideButton];
             
             // "Copy Logs" button definition
             UIButton* copyButton = [[UIButton alloc] initWithFrame:CGRectMake(100.0f, 55.0f, 80.0f, 40.0f)];
             [copyButton setTitle:@"Copy" forState:UIControlStateNormal];
             [copyButton addTarget:self action:@selector(copyLogs) forControlEvents:UIControlEventTouchUpInside];
             [viewController.view addSubview:copyButton];

             
             UIButton* closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 55.0f, 80.0f, 40.0f)];
             [closeButton setTitle:@"Close" forState:UIControlStateNormal];
             
             [closeButton addTarget:fromViewController action:@selector(dismissModalViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
             [viewController.view addSubview:closeButton];
             
             [fromViewController presentViewController:viewController animated:animated completion:nil];
         }
     }];
}



- (IBAction)SDKinfo:(id)sender {
    
    TSMiniWebBrowser *webBrowser = [[TSMiniWebBrowser alloc] initWithURL:[NSURL URLWithString:@"https://www.layar.com/products/custom-solutions/sdk/"]];
    
    webBrowser.delegate = self;
    webBrowser.showActionButton = YES;

   // webBrowser.showReloadButton = YES;
   [webBrowser setFixedTitleBarText:@"Layar SDK"]; // This has priority over "showPageTitleOnTitleBar".
    webBrowser.mode = TSMiniWebBrowserModeModal;
    
    webBrowser.barStyle = UIBarStyleDefault;
    
    webBrowser.modalDismissButtonTitle = @"Home";
    [self presentViewController:webBrowser animated:YES completion:nil];
    
}

- (IBAction)Layardownload:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                @"itms-apps://itunes.apple.com/us/app/layar-augmented-reality/id334404207?mt=8&uo=4"]];
    
}
- (IBAction)geoLaunch:(id)sender
{
//    [self openURL:@"layar://rsdevelopersharing" animated:YES];
    [self openURL:@"layar://stittvideorqgh" animated:YES];
    //[self openURL:@"layar://layarpartners2013" animated:YES];
   // [self openURL:@"layar://rsaltitude" animated:YES];
    
}

- (IBAction)Layarscan:(id)sender {
    [self setUpSDK];
    
    [self.layarSDK viewControllerForScanningWithCompletion:
     ^(UIViewController<LayarSDKViewController> *viewController)
     {
         viewController.tapToScanEnabled = NO;
         viewController.startScan;
         
         // "Close" button definition
         UIButton* closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 40.0f, 80.0f, 30.0f)];
         [closeButton setTitle:@"Close" forState:UIControlStateNormal];
         [closeButton addTarget:self action:@selector(dismissModalViewController) forControlEvents:UIControlEventTouchUpInside];
         [viewController.view addSubview:closeButton];

         
         // "Scan" button definition
         UIButton* scanButton = [[UIButton alloc] initWithFrame:CGRectMake(50.0f, 35.0f, 80.0f, 40.0f)];
         [scanButton setTitle:@"Scan" forState:UIControlStateNormal];
         [scanButton addTarget:viewController action:@selector(startScan) forControlEvents:UIControlEventTouchUpInside];
         [viewController.view addSubview:scanButton];
         
         // Logs
         logText = [[UITextView alloc] initWithFrame:CGRectMake(10, 65, 300, 200)];
         [logText setTextColor:[UIColor greenColor]];
         [logText setBackgroundColor:[UIColor clearColor]];
         [logText setFont:[UIFont fontWithName: @"Trebuchet MS" size: 12.0f]];
         [logText setEditable:false];
         [logText setSelectable:true];

         // "Hide Logs" button definition
         UIButton* hideButton = [[UIButton alloc] initWithFrame:CGRectMake(100.0f, 35.0f, 80.0f, 40.0f)];
         [hideButton setTitle:@"Hide" forState:UIControlStateNormal];
         [hideButton addTarget:self action:@selector(hideLogs) forControlEvents:UIControlEventTouchUpInside];
         [viewController.view addSubview:hideButton];
         
         
         // "Copy Logs" button definition
         UIButton* copyButton = [[UIButton alloc] initWithFrame:CGRectMake(150.0f, 35.0f, 80.0f, 40.0f)];
         [copyButton setTitle:@"Copy" forState:UIControlStateNormal];
         [copyButton addTarget:self action:@selector(copyLogs) forControlEvents:UIControlEventTouchUpInside];
         [viewController.view addSubview:copyButton];

         // "PopOut All" button definition
         UIButton* popButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 250.0f, 80.0f, 40.0f)];
         [popButton setTitle:@"Pop" forState:UIControlStateNormal];
         [popButton addTarget:self action:@selector(popAll) forControlEvents:UIControlEventTouchUpInside];
         [viewController.view addSubview:popButton];
         
         [viewController setShowPreviewImageWhenPoppedOut:NO];
         [viewController setEnable3DModeWhenPoppedOut:YES];
         viewController.trackingEnabledWhenPoppedOut = YES;
         viewController.tapToScanEnabled=YES;
         viewController.perform3DTiltWhenPoppedOut=NO;
         viewController.fitContentWhenPoppedOut=NO;
         
         self.viewController = viewController;
         
         NSString *logString = [NSString stringWithFormat:@"%s:\n layerSettingsAvailable: %@,\n mapViewAvailable: %@,\n popOutAvailable.: %@,\n tapToScanEnabled: %@,\n showPreviewImageWhenPoppedOut: %@,\n trackingEnabledWhenPoppedOut: %@,\n fitContentWhenPoppedOut: %@,\n perform3DTiltWhenPoppedOut: %@,\n enable3DModeWhenPoppedOut: %@,\n isCurrentlyPoppedOut: %@",
                                "ViewController properties",
                                viewController.layerSettingsAvailable ? @"YES" : @"NO",
                                (viewController.mapViewAvailable ? @"YES" : @"NO"),
                                viewController.popOutAvailable ? @"YES" : @"NO",
                                viewController.tapToScanEnabled ? @"YES" : @"NO",
                                viewController.showPreviewImageWhenPoppedOut ? @"YES" : @"NO",
                                viewController.trackingEnabledWhenPoppedOut ? @"YES" : @"NO",
                                viewController.fitContentWhenPoppedOut ? @"YES" : @"NO",
                                viewController.perform3DTiltWhenPoppedOut ? @"YES" : @"NO",
                                viewController.enable3DModeWhenPoppedOut ? @"YES" : @"NO",
                                viewController.isCurrentlyPoppedOut ? @"YES" : @"NO"
                                ];
         [self ShowLogs:logString onViewController:viewController];

         
         [self presentViewController:viewController animated:YES completion:
          ^{
          }];
         
     }];
    
}


- (void)dismissModalViewController
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)hideLogs
{
    dispatch_async(dispatch_get_main_queue(), ^{
        logText.hidden =!logText.hidden;
    });
}

- (void)copyLogs
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIPasteboard generalPasteboard].string = logText.text;
    });
}

- (void)popAll
{
    [self.viewController popOutAllTrackedTargets];
    
    // "Exit PopOut" button definition
    exitPopButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 290.0f, 80.0f, 40.0f)];
    [exitPopButton setTitle:@"Exit Pop" forState:UIControlStateNormal];
    [exitPopButton addTarget:self action:@selector(exitPopAll) forControlEvents:UIControlEventTouchUpInside];
    [self.viewController.view addSubview:exitPopButton];
    
    // "3DTilt" button definition
    threeDTiltPopButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 320.0f, 80.0f, 40.0f)];
    [threeDTiltPopButton setTitle:@"3D" forState:UIControlStateNormal];
    [threeDTiltPopButton addTarget:self action:@selector(swich3DTilt) forControlEvents:UIControlEventTouchUpInside];
    [self.viewController.view addSubview:threeDTiltPopButton];
    
    // "showPreviewImageWhenPoppedOut" button definition
    previewPopButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 350.0f, 80.0f, 40.0f)];
    [previewPopButton setTitle:@"Preview" forState:UIControlStateNormal];
    [previewPopButton addTarget:self action:@selector(swichPreview) forControlEvents:UIControlEventTouchUpInside];
    [self.viewController.view addSubview:previewPopButton];

    // "fitContentWhenPoppedOut" button definition
    fitPopButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 380.0f, 80.0f, 40.0f)];
    [fitPopButton setTitle:@"Fit" forState:UIControlStateNormal];
    [fitPopButton addTarget:self action:@selector(swichFit) forControlEvents:UIControlEventTouchUpInside];
    [self.viewController.view addSubview:fitPopButton];
    
    NSString *logString = [NSString stringWithFormat:@"%s","popOutAllTrackedTargets called"];
    [self ShowLogs: logString onViewController:self.viewController];
    
}

- (void)exitPopAll
{
    [self.viewController exitPopout];
    [exitPopButton removeFromSuperview];
    [threeDTiltPopButton removeFromSuperview];
    [previewPopButton removeFromSuperview];
    [fitPopButton removeFromSuperview];

    NSString *logString = [NSString stringWithFormat:@"%s","exitPopout called"];
    [self ShowLogs: logString onViewController:self.viewController];
}

- (void)swich3DTilt
{
    self.viewController.perform3DTiltWhenPoppedOut=!self.viewController.perform3DTiltWhenPoppedOut;
    
    NSString *logString = [NSString stringWithFormat:@"%s: %@","perform3DTiltWhenPoppedOut", self.viewController.perform3DTiltWhenPoppedOut? @"YES" : @"NO"];
    [self ShowLogs: logString onViewController:self.viewController];
}

- (void)swichPreview
{
    self.viewController.showPreviewImageWhenPoppedOut=!self.viewController.showPreviewImageWhenPoppedOut;
    
    NSString *logString = [NSString stringWithFormat:@"%s: %@","showPreviewImageWhenPoppedOut", self.viewController.showPreviewImageWhenPoppedOut? @"YES" : @"NO"];
    [self ShowLogs: logString onViewController:self.viewController];
}

- (void)swichFit
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.viewController.fitContentWhenPoppedOut=!self.viewController.fitContentWhenPoppedOut;
    });
    
    NSString *logString = [NSString stringWithFormat:@"%s: %@","fitContentWhenPoppedOut", self.viewController.fitContentWhenPoppedOut? @"YES" : @"NO"];
    [self ShowLogs: logString onViewController:self.viewController];
}


//re- attach content
- (void)layarSDK:(LayarSDK*)layarSdk didStartTrackingReferenceImage:(NSString*)referenceImageName
        withLayerObject:(NSDictionary*)layerObject
        onViewController:(UIViewController<LayarSDKViewController>*)viewController
    {
        [viewController exitPopout];
        NSString *logString = [NSString stringWithFormat:@"%s: %@","isCurrentlyPoppedOut", viewController.isCurrentlyPoppedOut ? @"YES" : @"NO"];
        [self ShowLogs: logString onViewController:self.viewController];
    
        [screenshotButton removeFromSuperview];
        screenshotButton = [[UIButton alloc] initWithFrame:CGRectMake(80.0f, 480.0f, 150.0f, 40.0f)];
        [screenshotButton setTitle:@"Take Screenshot" forState:UIControlStateNormal];
        [screenshotButton addTarget:viewController action:@selector(openScreenshotMode) forControlEvents:UIControlEventTouchUpInside];
        [viewController.view addSubview:screenshotButton];
        logString = [NSString stringWithFormat:@"%s, %@, %@","didStartTrackingReferenceImage with object activated", referenceImageName, layerObject];
        [self ShowLogs: logString onViewController:viewController];
  
        NSLog(@"Added Button");
    }

//methond to show detach mode as soon as user poitns away

- (void)layarSDK:(LayarSDK*)layarSdk didStopTrackingReferenceImage:(NSString*)referenceImageName
	withLayerObject:(NSDictionary*)layerObject
    onViewController:(UIViewController<LayarSDKViewController>*)viewController
    {
        [viewController popOutReferenceImageID:referenceImageName];//API implementation
        [screenshotButton removeFromSuperview];
        NSString *logString = [NSString stringWithFormat:@"%s: %@","isCurrentlyPoppedOut", viewController.isCurrentlyPoppedOut ? @"YES" : @"NO"];
        [self ShowLogs: logString onViewController:self.viewController];
    
        NSLog(@"Removed Button");
        logString = [NSString stringWithFormat:@"%s, referenceImageName: %@, layerObject: %@","didStopTrackingReferenceImage activated", referenceImageName, layerObject];
        [self ShowLogs:logString onViewController:viewController];

    }


//Stub implementations to test, whether the call is triggered.
- (void)layarSDK: (LayarSDK*)layarSdk didLaunchLayer:(NSString*)layerName
                        withTitle:(NSString *)layerTitle
     recognizedReferenceImageName:(NSString*)recognizedReferenceImageName
                 onViewController:(UIViewController*)viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, %@, %@","didLaunchLayer activated", layerTitle, layerName];
        [self ShowLogs: logString onViewController:viewController];
    }

- (void)layarSDK: (LayarSDK*)layarSdk didLaunchLayerWithLayerObject:(NSDictionary*)layerObject
    recognizedReferenceImageName:(NSString*)recognizedReferenceImageName
    onViewController:(UIViewController*)viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, %@","didLaunchLayerWithLayerObject activated", layerObject.description];
        [self ShowLogs: logString onViewController:viewController];
    }

- (void)layarSDK: (LayarSDK*)layarSdk didRecognizeQRCode:(NSString *) qrCodeText
    onViewController: (UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, %@","didRecognizeQRCode activated", qrCodeText];
        [self ShowLogs: logString onViewController:viewController];
    }

- (BOOL)layarSDK: (LayarSDK*)layarSdk shouldPerformVisualSearchForImage:(UIImage *) 	image
    onViewController: (UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, %f, %f","shouldPerformVisualSearchForImage activated", image.size.height, image.size.width];
        [self ShowLogs: logString onViewController:viewController];
        return YES;
    }

- (void)layarSDK: (LayarSDK*)layarSdk didReceiveVisualSearchResultsWithResponseCode:		(NSUInteger) 	responseCode
                         responseMessage:		(NSString *) 	responseMessage
      recognizedReferenceImagesForLayers:		(NSDictionary *) 	recognizedReferenceImagesForLayers
                        onViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, %lu, %@, %@","didReceiveVisualSearchResultsWithResponseCode activated", (unsigned long)responseCode, responseMessage, recognizedReferenceImagesForLayers];
        [self ShowLogs: logString onViewController:viewController];
    }

- (BOOL) layarSDK:		(LayarSDK *) 	layarSdk
    shouldShowNoResultsFoundForImage:		(UIImage *) 	image
                    onViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, %f, %f","shouldShowNoResultsFoundForImage activated", image.size.height, image.size.width];
        [self ShowLogs: logString onViewController:viewController];
        return YES;
    }

- (void) layarSDK:		(LayarSDK *) 	layarSdk
    didStartTrackingReferenceImage:		(NSString *) 	referenceImageName
                         withLayer:		(NSString *) 	layerName
                    withLayerTitle:		(NSString *) 	layerTitle
                  onViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, Image: %@, Layer: %@, Title: %@","didStartTrackingReferenceImage activated", referenceImageName, layerName, layerTitle];
        [self ShowLogs: logString onViewController:viewController];
    }


- (void) layarSDK:		(LayarSDK *) 	layarSdk
    didShowAugmentForPOI:		(NSString *) 	poiId
      referenceImageName:		(NSString *) 	referenceImageName
                   layer:		(NSString *) 	layerName
        onViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, poiId: %@, referenceImageName: %@, layerName: %@","didShowAugmentForPOI activated", poiId, referenceImageName, layerName];
        [self ShowLogs: logString onViewController:viewController];
    }

- (void) layarSDK:		(LayarSDK *) 	layarSdk
    didShowAugmentForPOIWithPOIObject:		(NSDictionary *) 	poiObject
                   referenceImageName:		(NSString *) 	referenceImageName
                          layerObject:		(NSDictionary *) 	layerObject
                     onViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, poiObject: %@, referenceImageName: %@, layerObject: %@","didShowAugmentForPOIWithPOIObject activated", poiObject, referenceImageName, layerObject];
        [self ShowLogs: logString onViewController:viewController];
    }

- (void) layarSDK:		(LayarSDK *) 	layarSdk
    didClickOnAugmentForPOI:		(NSString *) 	poiId
         referenceImageName:		(NSString *) 	referenceImageName
                      layer:		(NSString *) 	layerName
           onViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, poiId: %@, referenceImageName: %@, layerName: %@","didClickOnAugmentForPOI activated", poiId, referenceImageName, layerName];
        [self ShowLogs: logString onViewController:viewController];
    }



- (void) layarSDK:		(LayarSDK *) 	layarSdk
    didClickOnAugmentForPOIWithPOIObject:		(NSDictionary *) 	poiObject
                      referenceImageName:		(NSString *) 	referenceImageName
                             layerObject:		(NSDictionary *) 	layerObject
                        onViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, poiObject: %@, referenceImageName: %@, layerObject: %@","didClickOnAugmentForPOIWithPOIObject activated", poiObject, referenceImageName, layerObject];
        [self ShowLogs: logString onViewController:viewController];
    }

- (void) layarSDK:		(LayarSDK *) 	layarSdk
    didStartPlayingVideoForPOI:		(NSString *) 	poiId
            referenceImageName:		(NSString *) 	referenceImageName
                         layer:		(NSString *) 	layerName
              onViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, poiId: %@, referenceImageName: %@, layerName: %@","didStartPlayingVideoForPOI activated", poiId, referenceImageName, layerName];
        [self ShowLogs: logString onViewController:viewController];
    }



- (void) layarSDK:		(LayarSDK *) 	layarSdk
    didStartPlayingVideoForPOIWithPOIObject:		(NSDictionary *) 	poiObject
                         referenceImageName:		(NSString *) 	referenceImageName
                                layerObject:		(NSDictionary *) 	layerObject
                           onViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, poiObject: %@, referenceImageName: %@, layerObject: %@","didStartPlayingVideoForPOIWithPOIObject activated", poiObject, referenceImageName, layerObject];
        [self ShowLogs: logString onViewController:viewController];
    }

- (void) layarSDK:		(LayarSDK *) 	layarSdk
    didStartPlayingAudioForPOI:		(NSString *) 	poiId
            referenceImageName:		(NSString *) 	referenceImageName
                         layer:		(NSString *) 	layerName
              onViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, poiId: %@, referenceImageName: %@, layerName: %@","didStartPlayingAudioForPOI activated", poiId, referenceImageName, layerName];
        [self ShowLogs: logString onViewController:viewController];
    }



- (void) layarSDK:		(LayarSDK *) 	layarSdk
    didStartPlayingAudioForPOIWithPOIObject:		(NSDictionary *) 	poiObject
                         referenceImageName:		(NSString *) 	referenceImageName
                                layerObject:		(NSDictionary *) 	layerObject
                           onViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, poiObject: %@, referenceImageName: %@, layerObject: %@","didStartPlayingAudioForPOIWithPOIObject activated", poiObject, referenceImageName, layerObject];
        [self ShowLogs: logString onViewController:viewController];
    }

- (void) layarSDK:		(LayarSDK *) 	layarSdk
      didFocusPOI:		(NSString *) 	poiId
            layer:		(NSString *) 	layerName
 onViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, poiId: %@, layer: %@","didFocusPOI activated", poiId, layerName];
        [self ShowLogs: logString onViewController:viewController];
    }

- (void) layarSDK:		(LayarSDK *) 	layarSdk
    didFocusPOIWithPOIObject:		(NSDictionary *) 	poiObject
                 layerObject:		(NSDictionary *) 	layerObject
            onViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, poiObject: %@, layerObject: %@","didFocusPOIWithPOIObject activated", poiObject, layerObject];
        [self ShowLogs: logString onViewController:viewController];
    }

//switch from AR to geo
- (BOOL)layarSDK:(LayarSDK*)layarSdk shouldTriggerActionForPOI:(NSString*)poiId
    referenceImageName:(NSString*)referenceImageName
                 layer:(NSString*)layerName
                action:(NSDictionary*)action
      onViewController:(UIViewController*)viewController
    {
        //if ([((NSString*)action[@"uri"]) hasPrefix:@"layar://housesaround"])
        if ([((NSString*)action[@"uri"]) hasPrefix:@"layar://"])
        {
            //		[self dismissViewControllerAnimated:YES completion:
            //         ^{
            [self openURL:action[@"uri"] animated:YES fromViewController:viewController];
            //         }];
            NSString *logString = [NSString stringWithFormat:@"%s, referenceImageName: %@, layerName: %@, action: %@","shouldTriggerActionForPOI activated", referenceImageName, layerName, action];
            [self ShowLogs: logString onViewController:viewController];
            return NO;
        }
    
        NSString *logString = [NSString stringWithFormat:@"%s, referenceImageName: %@, layerName: %@, action: %@","shouldTriggerActionForPOI activated", referenceImageName, layerName, action];
        [self ShowLogs: logString onViewController:viewController];
        return YES;
    }

- (BOOL) layarSDK:		(LayarSDK *) 	layarSdk
    shouldTriggerActionForPOIWithPOIObject:		(NSDictionary *) 	poiObject
    referenceImageName:		(NSString *) 	referenceImageName
           layerObject:		(NSDictionary *) 	layerObject
                action:		(NSDictionary *) 	action
      onViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        if ([((NSString*)action[@"uri"]) hasPrefix:@"layar://"])
        {
            [self openURL:action[@"uri"] animated:YES fromViewController:viewController];
            NSString *logString = [NSString stringWithFormat:@"%s, referenceImageName: %@, layerObject: %@, action: %@","shouldTriggerActionForPOIWithPOIObject activated", referenceImageName, layerObject, action];
            [self ShowLogs: logString onViewController:viewController];
            return NO;
        }
        NSString *logString = [NSString stringWithFormat:@"%s, referenceImageName: %@, layerObject: %@, action: %@","shouldTriggerActionForPOIWithPOIObject activated", referenceImageName, layerObject, action];
        [self ShowLogs: logString onViewController:viewController];
        return YES;
    }

- (void) layarSDK:		(LayarSDK *) 	layarSdk
    didTriggerActionForPOI:		(NSString *) 	poiId
        referenceImageName:		(NSString *) 	referenceImageName
                     layer:		(NSString *) 	layerName
                    action:		(NSDictionary *) 	action
          onViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, poiId: %@, referenceImageName: %@, layerName: %@, action: %@","didTriggerActionForPOI activated", poiId, referenceImageName, layerName, action];
        [self ShowLogs: logString onViewController:viewController];
    }

- (void) layarSDK:		(LayarSDK *) 	layarSdk
    didTriggerActionForPOIWithPOIObject:		(NSDictionary *) 	poiObject
                     referenceImageName:		(NSString *) 	referenceImageName
                            layerObject:		(NSDictionary *) 	layerObject
                                 action:		(NSDictionary *) 	action
                       onViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s, poiObject: %@, referenceImageName: %@, layerName: %@, action: %@","didTriggerActionForPOIWithPOIObject activated", poiObject, referenceImageName, layerObject, action];
        [self ShowLogs: logString onViewController:viewController];
    }

- (void) layarSDK:		(LayarSDK *) 	layarSdk
    didEnterPopOutModeOnViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s","didEnterPopOutModeOnViewController activated"];
        [self ShowLogs: logString onViewController:viewController];
    }

- (void) layarSDK:		(LayarSDK *) 	layarSdk
    didExitPopOutModeOnViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s","didExitPopOutModeOnViewController activated"];
        [self ShowLogs: logString onViewController:viewController];
    }

- (void) layarSDK:		(LayarSDK *) 	layarSdk
    didEnterScreenshotModeOnViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s","didEnterScreenshotModeOnViewController activated"];
        [self ShowLogs: logString onViewController:viewController];
    }

- (void) layarSDK:		(LayarSDK *) 	layarSdk
    didExitScreenshotModeOnViewController:		(UIViewController< LayarSDKViewController > *) 	viewController
    {
        NSString *logString = [NSString stringWithFormat:@"%s","didExitScreenshotModeOnViewController activated"];
        [self ShowLogs: logString onViewController:viewController];
    }



@end
