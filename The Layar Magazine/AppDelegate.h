//
//  AppDelegate.h
//  The Layar Magazine
//
//  Created by Olga Beza on 10/11/13.
//  Copyright (c) 2013 Layar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
