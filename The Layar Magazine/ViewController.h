//
//  ViewController.h
//  The Layar Magazine
//
//  Created by Olga Beza on 10/11/13.
//  Copyright (c) 2013 Layar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LayarSDK/LayarSDK.h>
#import <TSMiniWebBrowser.h>

@interface ViewController : UIViewController <LayarSDKDelegate,TSMiniWebBrowserDelegate>
@property (nonatomic, retain) LayarSDK *layarSDK;

- (IBAction)SDKinfo:(id)sender;

- (IBAction)Layardownload:(id)sender;

- (IBAction)geoLaunch:(id)sender;

- (IBAction)Layarscan:(id)sender;

@property (nonatomic, retain) UIViewController<LayarSDKViewController> *viewController;
@end
